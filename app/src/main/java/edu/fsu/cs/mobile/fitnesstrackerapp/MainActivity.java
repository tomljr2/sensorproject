package edu.fsu.cs.mobile.fitnesstrackerapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends AppCompatActivity implements LocationListener,OnMapReadyCallback{

    static float avgSpeed;
    static float dist;
    static int counter;
    static Location prevLoc;
    GoogleMap map;
    Button workout;
    boolean workoutActive;
    static float speedSum;
    Marker marker;
    Polyline track;
    boolean workoutStart;
    boolean workoutEnd;
    String speedUnits;
    String distanceUnits;
    float timeBetweenLocations;
    float getTimeBetweenLocationsSum;
    float caloriesBurned;
    SensorManager stepManager;
    Sensor stepSensor;
    static int stepsInSensor;
    static int stepsAtReset;
    static int stepsSinceReset;
    boolean showSteps;

    SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            final TextView step = findViewById(R.id.step);
            final TextView stepUnit = findViewById(R.id.stepUnit);
                       stepsInSensor = (int) sensorEvent.values[0];
                       stepsSinceReset = stepsInSensor - stepsAtReset;
                       if(workoutActive && showSteps)
                       {
                           step.setText("" + stepsSinceReset);
                           stepUnit.setText(" steps");

                       }
                   }
        @Override
       public void onAccuracyChanged(Sensor sensor, int i) {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.action_bar);
        setSupportActionBar(toolbar);

        avgSpeed = 0;
        dist = 0;
        speedSum = 0;
        counter = 0;
        showSteps = false;

        stepManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        stepSensor = stepManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        stepManager.registerListener(sensorEventListener, stepSensor, SensorManager.SENSOR_DELAY_NORMAL);



        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        workout = findViewById(R.id.beginendworkout);
        workout.setBackgroundColor(getResources().getColor(R.color.clickable));
        workout.setBackgroundResource(R.drawable.custom_button);
        workoutActive = false;
        workoutStart = false;
        workoutEnd = false;
        speedUnits = "mph";
        distanceUnits = "mi";
        timeBetweenLocations = 0;
        getTimeBetweenLocationsSum = 0;
        caloriesBurned = 0;

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.getBestProvider(criteria, true);

        //This makes sure that the permissions are given. If not it'll just update the textview and
        //nothing will work until those permissions are given.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this,"Please give GPS permissions",Toast.LENGTH_SHORT);
            return;
        }

        //Check the location every 250 ms if the user has moved at least 1 meter. We may want to change
        //this to every 0 meters so that it will just always check every 1 second. That way it is on
        //a timer and not determined by distance.
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);

        workout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView speedTV = findViewById(R.id.speedTV);
                final TextView avgSpeedTV = findViewById(R.id.avgsTV);
                final TextView distanceTV = findViewById(R.id.distTV);
                final TextView elevationTV = findViewById(R.id.elevationTV);
                final TextView calBurnedTV = findViewById(R.id.calBurnedTV);
                final TextView stepTV = findViewById(R.id.stepTV);
                final TextView step = findViewById(R.id.step);
                final TextView stepUnit = findViewById(R.id.stepUnit);
                final TextView speed = findViewById(R.id.speed);
                final TextView speedUnit = findViewById(R.id.speedUnit);
                final TextView averageSpeed = findViewById(R.id.avgs);
                final TextView avgSpeedUnit= findViewById(R.id.avgsUnit);
                final TextView distance = findViewById(R.id.dist);
                final TextView calBurned = findViewById(R.id.calBurned);
                final TextView distanceUnit = findViewById(R.id.distUnit);
                final TextView elevation = findViewById(R.id.elevation);
                final TextView elevationUnit = findViewById(R.id.elevationUnit);
                final TextView calBurnedUnit = findViewById(R.id.calBurnedUnit);
                final Chronometer chronometer = findViewById(R.id.chronometer);

                if(workout.getText().equals("Begin Workout"))
                {
                    speedTV.setText("Speed");
                    avgSpeedTV.setText("Avg Speed");
                    distanceTV.setText("Distance");
                    elevationTV.setText("Elevation");
                    calBurnedTV.setText("Calories");
                    stepTV.setText("Steps");
                    workout.animate().xBy(400).setDuration(1000).start();
                    workoutActive = true;
                    workout.setText("End Workout");
                    stepsAtReset = stepsInSensor;
                    dist = 0;
                    avgSpeed = 0;
                    caloriesBurned = 0;
                    timeBetweenLocations = 0;
                    getTimeBetweenLocationsSum = 0;
                    chronometer.setTextColor(Color.BLACK);
                    chronometer.setFormat("%s");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    map.moveCamera(CameraUpdateFactory.zoomTo(20));
                    map.clear();
                    workoutStart = true;
                }
                else
                {
                    chronometer.setFormat("");
                    speedTV.setText("");
                    avgSpeedTV.setText("");
                    distanceTV.setText("");
                    elevationTV.setText("");
                    calBurnedTV.setText("");
                    speed.setText("");
                    averageSpeed.setText("");
                    distance.setText("");
                    elevation.setText("");
                    calBurned.setText("");
                    speedUnit.setText("");
                    avgSpeedUnit.setText("");
                    distanceUnit.setText("");
                    elevationUnit.setText("");
                    calBurnedUnit.setText("");
                    stepTV.setText("");
                    step.setText("");
                    stepUnit.setText("");
                    //chronometer.stop();
                    workout.animate().xBy(-400).setDuration(1000).start();
                    workout.setText("Begin Workout");
                    workoutActive = false;
                    map.moveCamera(CameraUpdateFactory.zoomTo(15));
                    workoutEnd = true;
                    showSteps = false;
                }
            }
        });
    }
    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onLocationChanged(Location location) {
        // Currently when the location changes all we do is update a text view, later on, we'll want to
        // use the current lat and long to plot points on a map probably every five to ten seconds instead
        // of every 1/4 second that this updates. We could simply have a counter that increments every
        // time this function is triggered and then mod that by something like 40 if we want it to plot
        // every ten seconds.
        final TextView speed = findViewById(R.id.speed);
        final TextView speedUnit = findViewById(R.id.speedUnit);
        final TextView averageSpeed = findViewById(R.id.avgs);
        final TextView avgSpeedUnit= findViewById(R.id.avgsUnit);
        final TextView distance = findViewById(R.id.dist);
        final TextView distanceUnit = findViewById(R.id.distUnit);
        final TextView elevation = findViewById(R.id.elevation);
        final TextView elevationUnit = findViewById(R.id.elevationUnit);
        final TextView calBurned = findViewById(R.id.calBurned);
        final TextView calBurnedUnit = findViewById(R.id.calBurnedUnit);
        final Chronometer chronometer = findViewById(R.id.chronometer);
        final TextView step = findViewById(R.id.step);
        final TextView stepUnit = findViewById(R.id.stepUnit);

        if(workoutEnd && prevLoc != null)
        {
            map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.end))
                    .position(new LatLng(location.getLatitude(),location.getLongitude())));
            workoutEnd = false;
        }
        else if(workoutEnd && prevLoc == null)
        {
            map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.end))
                    .position(new LatLng(location.getLatitude(),location.getLongitude())));
        }

        if(prevLoc != null && workoutActive && location.getSpeed() != 0)
            track = map.addPolyline(new PolylineOptions()
                    .add(new LatLng(prevLoc.getLatitude(),prevLoc.getLongitude()))
                    .add(new LatLng(location.getLatitude(), location.getLongitude())));

        speedSum += location.getSpeed();

        if(location.getSpeed() != 0)    //Only update the average speed if the person is actually moving
        {
            marker.remove();
            counter++;
            avgSpeed = speedSum / counter;
            marker = map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                    .position(new LatLng(location.getLatitude(),location.getLongitude())));
            map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude())));
        }

        if(workoutActive)
        {
            showSteps = true;
            getTimeBetweenLocationsSum += timeBetweenLocations;
            timeBetweenLocations = ((SystemClock.elapsedRealtime() - chronometer.getBase()) - getTimeBetweenLocationsSum);

            switch(speedUnits)
            {
                case "m/s":
                    speed.setText(String.format("%.02f", location.getSpeed()));
                    averageSpeed.setText(String.format("%.02f", avgSpeed));
                    speedUnit.setText("m/s");
                    avgSpeedUnit.setText("m/s");
                    break;
                case "mph":
                    speed.setText(String.format("%.02f", mpstomph(location.getSpeed())));
                    averageSpeed.setText(String.format("%.02f", mpstomph(avgSpeed)));
                    speedUnit.setText("mph");
                    avgSpeedUnit.setText("mph");
                    break;
                case "km/h":
                    speed.setText(String.format("%.02f", mpstokmh(location.getSpeed())));
                    averageSpeed.setText(String.format("%.02f", mpstokmh(avgSpeed)));
                    speedUnit.setText("km/h");
                    avgSpeedUnit.setText("km/h");
                    break;
            }

            switch (distanceUnits)
            {
                case "m":
                    distance.setText(String.format("%.02f", (float) dist));
                    elevation.setText(String.format("%.02f", location.getAltitude()));
                    distanceUnit.setText("m");
                    elevationUnit.setText("m");

                    break;
                case "mi":
                    distance.setText(String.format("%.02f", mtomi(dist)));
                    elevation.setText(String.format("%.02f", mtomi(location.getAltitude())));
                    distanceUnit.setText("mi");
                    elevationUnit.setText("mi");
                    break;
                case "km":
                    distance.setText(String.format("%.02f", mtokm(dist)));
                    elevation.setText(String.format("%.02f", mtokm(location.getAltitude())));
                    distanceUnit.setText("km");
                    elevationUnit.setText("km");
                    break;
            }

            if(location.getSpeed() > 0 && location.getSpeed() <= 0.759968)
                caloriesBurned += ((2.3 * 79.3787 * timeBetweenLocations) / 1800000);
            else if(location.getSpeed() > 1.1176 && location.getSpeed() <= 1.34112)
                caloriesBurned += ((2.9 * 79.3787 * timeBetweenLocations) / 1800000);
            else if(location.getSpeed() > 1.34112 && location.getSpeed() <= 1.51994)
                caloriesBurned += ((3.6 * 79.3787 * timeBetweenLocations) / 1800000);
            else if(location.getSpeed() > 1.51994 && location.getSpeed() <= 2.01168)
                caloriesBurned += ((5 * 79.3787 * timeBetweenLocations) / 1800000);
            else if(location.getSpeed() > 2.01168 && location.getSpeed() <= 2.45872)
                caloriesBurned += ((7 * 79.3787 * timeBetweenLocations) / 1800000);
            else if(location.getSpeed() > 2.45872)
                caloriesBurned += ((8 * 79.3787 * timeBetweenLocations) / 1800000);

            calBurned.setText(String.format("%.02f", caloriesBurned));
            calBurnedUnit.setText("cal");
        }



        if(prevLoc != null && location.getSpeed() != 0) //If the user is not moving, don't add to distance
            dist += prevLoc.distanceTo(location);

        if(location.getSpeed() != 0 || workoutStart)
            prevLoc = location;


        if(workoutStart)
        {
            map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.start))
                    .position(new LatLng(location.getLatitude(),location.getLongitude())));
            workoutStart = false;
            step.setText("0");
            stepUnit.setText(" steps");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(30.4383,-84.2807)));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(15));
        map = googleMap;
        marker = map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .position(new LatLng(30.4383,-84.2807)));
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(MainActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }
    //Some required methods since the activity implements locationListener
    @Override public void onStatusChanged(String provider, int status, Bundle extras) {}
    @Override public void onProviderEnabled(String provider) {}

    //Helper functions to convert the speed from m/s to mph and km/h and to convert distance in
    // meters to miles and km
    float mpstomph (float mps)
    { return (float) (mps * 2.2369356); }

    float mpstokmh (float mps)
    { return (float) (mps * 3.6); }

    float mtomi (float m)
    { return (float) (m * 0.000621371); }

    float mtokm (float m)
    { return (float) (m * 0.001); }

    double mtomi (double m)
    { return (m * 0.000621371); }

    double mtokm (double m)
    { return (m * 0.001); }


    // OPTIONS MENU

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getTitle().equals("mph"))
            speedUnits = "mph";
        else if(item.getTitle().equals("km/h"))
            speedUnits = "km/h";
        else if(item.getTitle().equals("m/s"))
            speedUnits = "m/s";
        else if(item.getTitle().equals("mi"))
            distanceUnits = "mi";
        else if(item.getTitle().equals("km"))
            distanceUnits = "km";
        else if(item.getTitle().equals("m"))
            distanceUnits = "m";

        return super.onOptionsItemSelected(item);
    }

}





